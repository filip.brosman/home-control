#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <thread>
#include <OneWire.h>
#include <DS18B20Driver.h>
#include <messageBox.h>

#include <stdlib.h>
#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;
constexpr auto BSENSORFILE = "sensors.sns";
constexpr auto SENSORFILE = "sensors.txt";

class TempControlError
{
private:
    string msg;
public:
    TempControlError(string msg) : msg{ msg } {};
    string what() const;
};

class TempControl
{
private:
    sql::Driver* driver;
    sql::Connection* con;
    sql::Statement* stmt;
    sql::ResultSet* res;

    OneWireDevices owDriver;
    vector<DS18B20Device*> container;
    string names[4]
    {
        "Garaz",
        "Vonku",
        "Pivnica",
        "Filip Izba"
    };
    bool controlProcess{ true };

    void initOW();
    int connectDB();
    void makeAllDevicesInDB();
    void saveToDB(DS18B20Device* device);

    void runControl();

public:
    TempControl();
    ~TempControl();

    thread start();
    void stop();
};

