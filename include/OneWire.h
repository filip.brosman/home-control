#pragma once
#include <stdio.h>
#include <bcm2835.h>
#include <sched.h>
#include <sys/mman.h>
#include <iostream>
#include <string>

using namespace std;

class OneWireDevices
{
private:
    static const uint8_t pin{ RPI_GPIO_P1_07 }; //Dafaultne nastavený pin na 7
    
public:
    uint64_t serial[15];
    int numberOfDevices;

protected: 
    uint64_t mSerial;
    string mName{ "Default name" };
    float mTemp;      

    static void writeBit(uint8_t b);
    static uint8_t readBit();
    static void writeByte(uint8_t byte);
    static uint8_t readByte();
    static int presence();
    static uint8_t crc8(uint8_t* data, uint8_t len);
    static int convert();
    static void matchROM(uint64_t device);
    
    int oneWireScan(uint64_t serial[]);    
    void scanBus(uint64_t serial[]);

public:
    OneWireDevices();
    OneWireDevices(uint8_t p);
    ~OneWireDevices();

    void setPin(uint8_t p);
};
