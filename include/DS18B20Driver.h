#pragma once
#include <OneWire.h>
#include <iomanip>

using namespace std;

class DS18B20Device : OneWireDevices
{
public:
    DS18B20Device(uint64_t sr, string name);    
    DS18B20Device(uint64_t sr);    
    ~DS18B20Device();
private:
    float getTempSkipRom();
    float getDeviceTemperature(uint64_t device);
 
public:
    float readTemp();
    float getTemp();
    uint64_t getSerial();
    void printTemp();       
};
