#pragma once
#include <iostream>
#include <iomanip>
#include <string.h>

using namespace std;
class MessageBox
{
public:
    static void info(string msg);
    static void warning(string msg);
    static void error(string msg);
    static void error(string msg, int32_t errNum);
};

