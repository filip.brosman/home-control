#pragma once
#include <TempControl.h>

class BaseControl
{
private:
    TempControl tempCont;
public:
    BaseControl();
    ~BaseControl();
    void runControl();
};

