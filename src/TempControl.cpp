#include "TempControl.h"


TempControl::TempControl()
{
    if (connectDB())
        throw TempControlError("Uneble to connect database on localhost.");
}

int TempControl::connectDB()
{
    try {
        /* Create a connection */
        driver = get_driver_instance();
        con = driver->connect("tcp://127.0.0.1:3306", "filip", "filip");
        /* Connect to the MySQL test database */
        con->setSchema("home_control");

        stmt = con->createStatement();      
    }
    catch (sql::SQLException& e) {
        cerr << "# ERR: SQLException in " << __FILE__;
        cerr << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cerr << "# ERR: " << e.what();
        cerr << " (MySQL error code: " << e.getErrorCode();
        cerr << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    return EXIT_SUCCESS;
}
void TempControl::saveToDB(DS18B20Device* device)
{
    string serial = to_string(device->getSerial());
    string value = to_string(device->getTemp());
    string query{ "insert into data( sensor_id, value) values(" + serial + "," + value + ");" };
    stmt->execute(query);
}

void TempControl::makeAllDevicesInDB()
{
    for (auto item : container)
    {
        try {
            string serial = to_string(item->getSerial());
            string query{ "insert into sensors(sensor_id) values(" +serial+");" };
            stmt->execute(query);
            MessageBox::info("Saving to db: " + query);
        }
        catch (sql::SQLException& e)
        {
            MessageBox::warning(e.what());
            continue;
        }
    }
}

TempControl::~TempControl()
{
    for (int i = 0; i < owDriver.numberOfDevices; i++)
        delete container[i];

    if (!res)
        delete res;
    if (!stmt)
        delete stmt;
    if(!con)
        delete con;
}

void TempControl::initOW()
{
    MessageBox::info("Po�et cidiel: " + to_string(owDriver.numberOfDevices));
    for (int i = 0; i < owDriver.numberOfDevices; i++)
        container.push_back(new DS18B20Device{ owDriver.serial[i] , names[i] });
}

void TempControl::runControl()
{
    initOW();
    makeAllDevicesInDB();
    while (controlProcess)
    {
        for (auto item : container)
        {
            item->readTemp();
            item->printTemp();
            saveToDB(item);
        }
        cout << "\n";

        this_thread::sleep_for(std::chrono::milliseconds(5000));
    }        
}

thread TempControl::start()
{
   return thread([this] { this->runControl(); });
}

void TempControl::stop()
{
    controlProcess = false;
}

string TempControlError::what() const
{
    return msg;
}
