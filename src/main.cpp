#include <iostream>
#include <BaseControl.h>


int main(void)
{	
	try {
		BaseControl baseControl;
		baseControl.runControl();
	}
	catch (int err)
	{
		MessageBox::error("Error heandled: ", err);
	}
	catch (TempControlError& err)
	{
		MessageBox::error(err.what());
	}
	return 0;
}