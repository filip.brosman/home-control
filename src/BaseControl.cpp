#include "BaseControl.h"
#include <iostream>
#include <thread>
#include <chrono>  
#include <messageBox.h>

using namespace std;

BaseControl::BaseControl()
{
}

BaseControl::~BaseControl()
{
}

void BaseControl::runControl()
{
    MessageBox::info("Application startded");
    MessageBox::info("To stop press enter");
    thread t1 = tempCont.start();

    cin.get();   
    tempCont.stop();
    MessageBox::warning("Stoped! Please wait!");
    t1.join();
    MessageBox::info("Temp Control stopped");
}
