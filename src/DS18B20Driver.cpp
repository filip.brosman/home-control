#include <DS18B20Driver.h>

DS18B20Device::DS18B20Device(uint64_t sr, string name)
{
    mSerial = sr ;
    mName = name;
}

DS18B20Device::DS18B20Device(uint64_t sr)
{
    mSerial = sr;
}

DS18B20Device::~DS18B20Device()
{
}

float DS18B20Device::getTempSkipRom() {
    if (presence() == 1) return -1000;
    writeByte(0xCC);
    convert();
    presence();
    writeByte(0xCC);
    writeByte(0xBE);
    int i;
    uint8_t data[9];
    for (i = 0; i < 9; i++) {
        data[i] = readByte();
    }
    uint8_t crc = crc8(data, 9);
    if (crc != 0) return -2000;
    int t1 = data[0];
    int t2 = data[1];
    int32_t temp1 = (t2 << 8 | t1);
    float temp = (float)temp1 / 16;
    return temp;
}

float DS18B20Device::getDeviceTemperature(uint64_t device)
{
    if (presence() == 1) return -1000;
    matchROM(device);
    convert();
    presence();
    matchROM(device);
    writeByte(0xBE);

    uint8_t data[9];
    int i;
    for (i = 0; i < 9; i++) {
        data[i] = readByte();
    }
    uint8_t crc = crc8(data, 9);
    if (crc != 0) return -2000;
    int t1 = data[0];
    int t2 = data[1];
    int32_t temp1 = (t2 << 8 | t1);
    float temp = (float)temp1 / 16;
    return temp;
}

float DS18B20Device::readTemp()
{
    do {
        mTemp = getDeviceTemperature(mSerial);
    } while (mTemp == -2000 || mTemp == -1000);
    return mTemp;
}

float DS18B20Device::getTemp()
{
    return mTemp;
}

uint64_t DS18B20Device::getSerial()
{
    return mSerial;
}

void DS18B20Device::printTemp()
{
    cout << setw(12) << setfill('.') << mName << getTemp() <<"'C " << "\n";
}
