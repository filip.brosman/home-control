#include "messageBox.h"

void MessageBox::info(string msg)
{
    cout << setw(12);
    cout << left;
    cout << "Info: " << msg << "\n\n";
}

void MessageBox::warning(string msg)
{
    cout << setw(12);
    cout << left;
    cout << "!Warning: " << msg << "\n\n";
}

void MessageBox::error(string msg)
{
    cerr << setw(12);
    cerr << left;
    cerr << "!!!Error: " << msg << "\n\n";
}

void MessageBox::error(string msg, int32_t errNum)
{
    cerr << setw(12);
    cerr << left;
    cerr << "!!!Error: " << msg << " #" << errNum << "\n\n";
}
