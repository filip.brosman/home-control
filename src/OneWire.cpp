#include <OneWire.h>
#include <iostream>

OneWireDevices::OneWireDevices()
{
    const struct sched_param priority = { 5 };
    sched_setscheduler(0, SCHED_FIFO, &priority);
    mlockall(MCL_CURRENT | MCL_FUTURE);
    if (!bcm2835_init())
        return;
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
    scanBus(serial);
}

//OneWireDriver::OneWireDriver(uint8_t p) : pin{ p }
//{
//    const struct sched_param priority = { 5 };
//    sched_setscheduler(0, SCHED_FIFO, &priority);
//    mlockall(MCL_CURRENT | MCL_FUTURE);
//    if (!bcm2835_init())
//        return;
//    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
//    scanBus(serial);
//}

OneWireDevices::~OneWireDevices()
{
    bcm2835_close();
}

int OneWireDevices::presence() {
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, LOW);
    bcm2835_delayMicroseconds(480);
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
    bcm2835_delayMicroseconds(70);
    uint8_t b = bcm2835_gpio_lev(pin);
    bcm2835_delayMicroseconds(410);
    return b;
}

void OneWireDevices::writeBit(uint8_t b) {

    int delay1, delay2;
    if (b == 1) {
        delay1 = 6;
        delay2 = 64;
    }
    else {
        delay1 = 80;
        delay2 = 10;
    }
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, LOW);
    bcm2835_delayMicroseconds(delay1);
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
    bcm2835_delayMicroseconds(delay2);
}

uint8_t OneWireDevices::readBit() {
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, LOW);
    bcm2835_delayMicroseconds(6);
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
    bcm2835_delayMicroseconds(9);
    uint8_t b = bcm2835_gpio_lev(pin);
    bcm2835_delayMicroseconds(55);
    return b;
}

void OneWireDevices::writeByte(uint8_t byte) {
    uint8_t i;
    for (i = 0; i < 8; i++) {
        if (byte & 1) {
            writeBit(1);
        }
        else {
            writeBit(0);
        }
        byte = byte >> 1;
    }
}

uint8_t OneWireDevices::readByte() {
    uint8_t byte = 0;
    int i;
    for (i = 0; i < 8; i++) {
        byte = byte | readBit() << i;
    };
    return byte;
}

uint8_t OneWireDevices::crc8(uint8_t* data, uint8_t len) {

    uint8_t i;
    uint8_t j;
    uint8_t temp;
    uint8_t databyte;
    uint8_t crc = 0;
    for (i = 0; i < len; i++) {
        databyte = data[i];
        for (j = 0; j < 8; j++) {
            temp = (crc ^ databyte) & 0x01;
            crc >>= 1U;
            if (temp)
                crc ^= 0x8C;

            databyte >>= 1U;
        }
    }
    return crc;
}

int OneWireDevices::convert() {
    int i;
    writeByte(0x44);
    for (i = 0; i < 1000; i++) 
    {
        bcm2835_delayMicroseconds(100000);
        if (readBit() == 1)break;
    }
    return i;
}

int OneWireDevices::oneWireScan(uint64_t serial[]) {
    static int bitcount = 0;
    static int deviceCount = 0;

    if (bitcount > 63)
    {
        bitcount = 0;
        deviceCount++;
        return deviceCount;
    }

    if (bitcount == 0)
    {
        if (presence() == 1)
        {
            bitcount = 0;
            return deviceCount;
        }
        deviceCount = 0;
        serial[deviceCount] = 0;
        writeByte(0xF0);
    };

    int b1 = readBit();
    int b2 = readBit();

    if (b1 == 0 && b2 == 1)
    {
        serial[deviceCount] >>= 1;
        writeBit(0);
        bitcount++;
        oneWireScan(serial);
    };

    if (b1 == 1 && b2 == 0) {
        serial[deviceCount] >>= 1;
        serial[deviceCount] |= 0x8000000000000000LL;
        writeBit(1);
        bitcount++;
        oneWireScan(serial);

    };

    if (b1 == 1 && b2 == 1) {
        bitcount = 0;
        return deviceCount;
    };

    if (b1 == 0 && b2 == 0)
    {
        serial[deviceCount] >>= 1;
        writeBit(0);
        int bitposition = bitcount;
        bitcount++;
        oneWireScan(serial);

        bitcount = bitposition;
        if (presence() == 1)
        {
            bitposition = 0;
            return 0;
        }

        writeByte(0xF0);
        uint64_t temp = serial[deviceCount - 1] | (0x1LL << (bitcount));
        int i;
        uint64_t bit;
        for (i = 0; i < bitcount + 1; i++)
        {
            bit = temp & 0x01LL;
            temp >>= 1;
            b1 = readBit();
            b2 = readBit();
            writeBit(bit);
            serial[deviceCount] >>= 1;
            serial[deviceCount] |= (bit << 63);
        }
        bitcount++;
        oneWireScan(serial);
    };
    return deviceCount;
}

void OneWireDevices::scanBus(uint64_t serial[])
{
    int j;
    uint8_t* code;
    int crc, d;
    for (j = 0; j < 15; j++)
    {
        d = oneWireScan(serial);
        if (d < 1)continue;
        crc = 0;
        int i;
        for (i = 0; i < d; i++) {
            code = (uint8_t*)&serial[i];
            crc += crc8(code, 8);
        }        
        fflush(stdout);
        if (crc == 0)break;
        bcm2835_delayMicroseconds(3000);
    }
    numberOfDevices = d;
}

//void OneWireDriver::setPin(uint8_t p)
//{
//    pin = p;
//    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
//}

void OneWireDevices::matchROM(uint64_t device) {
    writeByte(0x55);
    int i, bit;
    for (i = 0; i < 64; i++) 
    {
        bit = device & 0x01;
        device >>= 1;
        writeBit(bit);
    }
}